﻿class GameLoader {

    renderCanvas: HTMLCanvasElement;
    engine: BABYLON.Engine;
    scene: BABYLON.Scene;

    constructor(renderCanvas: HTMLCanvasElement) {
        this.renderCanvas = renderCanvas;
        this.engine = new BABYLON.Engine(this.renderCanvas, true);
        this.scene = this.createScene();
        this.engine.runRenderLoop(() => this.onRender());
        window.addEventListener("resize", (ev: MouseEvent) => this.onResize(ev));
    }

    createScene(): BABYLON.Scene {
        var scene = new BABYLON.Scene(this.engine);
        scene.clearColor = new BABYLON.Color3(1, 1, 1);

        var camera = new BABYLON.ArcRotateCamera("Camera", 1.0, 1.0, 12, BABYLON.Vector3.Zero(), scene);
        camera.attachControl(this.renderCanvas, false);

        var light = new BABYLON.HemisphericLight("hemi", new BABYLON.Vector3(0, 1, 0), scene);
        light.groundColor = new BABYLON.Color3(0.5, 0, 0.5);

        var box = BABYLON.Mesh.CreateBox("mesh", 3, scene);
        box.showBoundingBox = true;

        var material = new BABYLON.StandardMaterial("std", scene);
        material.diffuseColor = new BABYLON.Color3(0.5, 0, 0.5);

        box.material = material;
        return scene;
    }

    onRender(): void {
        this.scene.render();
    }

    onResize(ev:MouseEvent): void {
        this.engine.resize();
    }
}

window.onload = () => {
    var el = <HTMLCanvasElement>document.getElementById('renderCanvas');
    var gameLoader = new GameLoader(el);
};