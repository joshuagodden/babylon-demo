﻿class BaseObject {

    id: number;
    scene: BABYLON.Scene;

    constructor(scene:BABYLON.Scene) {
        this.id         = Math.random() * 10000;
        this.scene      = scene;
    }

    update() {
    }

    draw() {
    }

    destroy() {
        this.id = null;
    }
}