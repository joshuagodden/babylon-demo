﻿class MeshObject extends BaseObject {

    mesh: BABYLON.Mesh;

    constructor(scene:BABYLON.Scene) {
        super(scene);
        this.mesh = BABYLON.Mesh.CreateBox(this.id.toString(), 3, this.scene);
    }

    update() {
        super.update();
    }

    draw() {
        super.draw();
    }

    destroy() {
        super.destroy();
    }
} 