﻿var GameLoader = (function () {
    function GameLoader(renderCanvas) {
        var _this = this;
        this.renderCanvas = renderCanvas;
        this.engine = new BABYLON.Engine(this.renderCanvas, true);
        this.scene = this.createScene();
        this.engine.runRenderLoop(function () {
            return _this.onRender();
        });
        window.addEventListener("resize", function (ev) {
            return _this.onResize(ev);
        });
    }
    GameLoader.prototype.createScene = function () {
        var scene = new BABYLON.Scene(this.engine);
        scene.clearColor = new BABYLON.Color3(1, 1, 1);

        var camera = new BABYLON.ArcRotateCamera("Camera", 1.0, 1.0, 12, BABYLON.Vector3.Zero(), scene);
        camera.attachControl(this.renderCanvas, false);

        var light = new BABYLON.HemisphericLight("hemi", new BABYLON.Vector3(0, 1, 0), scene);
        light.groundColor = new BABYLON.Color3(0.5, 0, 0.5);

        var box = BABYLON.Mesh.CreateBox("mesh", 3, scene);
        box.showBoundingBox = true;

        var material = new BABYLON.StandardMaterial("std", scene);
        material.diffuseColor = new BABYLON.Color3(0.5, 0, 0.5);

        box.material = material;
        return scene;
    };

    GameLoader.prototype.onRender = function () {
        this.scene.render();
    };

    GameLoader.prototype.onResize = function (ev) {
        this.engine.resize();
    };
    return GameLoader;
})();

window.onload = function () {
    var el = document.getElementById('renderCanvas');
    var gameLoader = new GameLoader(el);
};
//# sourceMappingURL=app.js.map
